<style>
    #submitBtn
    {
        background: #BAAA76;
        color: black;
    }
    #payment
    {
        background: rgba(37,37,36,0.6);
        padding: 20px;
    }
    .payment-icon
    {
        font-size: 3em;
        padding-right: 0.5em;
    }
</style>
<div id="payment">
    <span><i class="fa fa-circle-o"></i></span><p style="font-size: 1.1em;padding-left:0.5em;display: inline-block"><strong>PAIEMENT EN LIGNE</strong></p>
    <br>
    <i class="fa fa-cc-stripe payment-icon" aria-hidden="true"></i>
    <i class="fa fa-cc-visa payment-icon" aria-hidden="true"></i>
    <i class="fa fa-cc-mastercard payment-icon" aria-hidden="true"></i>
{!! Form::open(['url' => '/payment', 'data-parsley-validate', 'id' => 'payment-form','method'=>'post']) !!}
<div class="form-group" id="cc-group">
    {!! Form::label(null, 'Credit card number:') !!}
    {!! Form::text(null, null, [
        'class'                         => 'form-control',
        'required'                      => 'required',
        'data-stripe'                   => 'number',
        'data-parsley-type'             => 'number',
        'maxlength'                     => '16',
        'data-parsley-trigger'          => 'change focusout',
        'data-parsley-class-handler'    => '#cc-group'
        ]) !!}
</div>

<div class="form-group" id="ccv-group">
    {!! Form::label(null, 'Card Validation Code (3 or 4 digit number):') !!}
    {!! Form::text(null, null, [
        'class'                         => 'form-control',
        'required'                      => 'required',
        'data-stripe'                   => 'cvc',
        'data-parsley-type'             => 'number',
        'data-parsley-trigger'          => 'change focusout',
        'maxlength'                     => '4',
        'data-parsley-class-handler'    => '#ccv-group'
        ]) !!}
</div>

        <div class="form-group" id="exp-m-group">
            {!! Form::label(null, 'Ex. Month') !!}
            {!! Form::selectMonth(null, null, [
                'class'                 => 'form-control',
                'required'              => 'required',
                'data-stripe'           => 'exp-month'
            ], '%m') !!}
        </div>
        <div class="form-group" id="exp-y-group">
            {!! Form::label(null, 'Ex. Year') !!}
            {!! Form::selectYear(null, date('Y'), date('Y') + 10, null, [
                'class'             => 'form-control',
                'required'          => 'required',
                'data-stripe'       => 'exp-year'
                ]) !!}
        </div>

<div class="form-group">
    {!! Form::submit('VALIDAE!', ['class' => 'btn btn-order', 'id' => 'submitBtn', 'style' => 'margin-bottom: 10px;']) !!}
</div>

<div class="row">
    <div class="col-md-12">
        <span class="payment-errors" style="color: red;margin-top:10px;"></span>
    </div>
</div>
@if($data['livraison-type']=='emporter')
    <input type="hidden" name="type" value="emporter">
    <input type="hidden" name="address" value="{{$data['address']}}">
    <input type="hidden" name="lat" value="{{$data['lat']}}">
    <input type="hidden" name="lng" value="{{$data['lng']}}">
    @else
    <input type="hidden" name="type" value="livraison">
    <input type="hidden" name="relais" value="{{$data['relais']}}">
    <input type="hidden" name="address" value="{{$data['relais_place']}}">
    @endif
<input type="hidden" name="date" value="{{$data['date']}}">
<input type="hidden" name="time" value="{{$data['time']}}">
<input type="hidden" name="price" value="{{$total}}">
</div>
{!! Form::close() !!}