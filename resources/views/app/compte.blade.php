@extends('app.app_template')
@section('content')
    <style>
        .label {
            color: #BAAA76;
            font-size: 14px;
            width: 100px;
            height: 40px !important;
        }
        .submit
        {
            display: block;
            background: #BAAA76;
            color: black;
            border: 0pt;
            box-shadow: none;
            border-radius: 5px;
        }
        .submit:hover,.submit-form:hover{
            opacity:0.5;
        }

    </style>
    <div class="container-fluid" style="min-height: 600px;background: black;color: #BAAA76">
        <div id="content" style="width: 1000px;margin: 0px auto">
            <div id="infor" style="width: 25%;float:left;">
                <h4>Facteur</h4>
                <ul>
                    @foreach($orders as $order)
                       <li style="list-style: none">
                           <form action="/pdf" method="post">
                            {!! csrf_field() !!}
                            <input type="hidden" name="id" value="{{$order->id}}">
                            <a href="/pdf" style="display: inline-block;text-decoration: none;color: #BAAA76;font-size: 1.2em">{{$order->facture}}</a>
                            <span type="submit" style="cursor: pointer;" class="submit-form"><i class="fa fa-download" aria-hidden="true"></i></span>

                        </form>
                       </li>
                    @endforeach
                </ul>

            </div>
            <div style="width: 70%;float:left;">
                <section>
                    <h3>MES INFORMATIONS PERSONNELLES</h3>
                    <p>Prénom / Nom: <span
                                style="margin-left: 20px;">{{Auth::user()->prenom}} {{Auth::user()->nom}}</span></p>
                    <p>E-mail: <span style="margin-left: 20px;">{{Auth::user()->email}}</span></p>
                    <button class="submit" id="change" style="margin: 10px 0px;">Change password</button>
                    <form id="change_form" action="" style="width: 467px;display: none">
                        <div class="form-group">
                            <label for="">Old password</label>
                            <input type="password" name="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">New password</label>
                            <input type="password" name="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <button class="submit">Validate</button>
                        </div>
                    </form>
                </section>
                <section>
                    <form action="{{url('/saveInfo')}}" method="post">
                        {{csrf_field()}}
                        <div style="width: 467px; border-top:thin solid #baaa76; "><h3>Mon Adresse</h3></div>
                        <div class="form-group" style="width: 467px;">
                            <label for="">Nom de société</label>
                            <input class=" form-control" type="text" name="societe"
                                   value="{{Auth::user()->societe}}">
                        </div>

                        <div class="form-group" style="width: 467px;">
                            <label for="">Téléphone</label>
                            <input class=" form-control" type="text" name="tel" required
                                   value="{{Auth::user()->tel}}">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="submit">ENREGISTRER</button>
                        </div>
                        </form>
                        <label for="">Adresse</label>
                        @foreach(Auth::user()->hasAddress as $address)
                            <li style="width: 467px;height: 33px;display: block">
                                <form action="/address/{{$address->id}}" method="post">
                                    {{csrf_field()}}
                                    <input type="hidden" name="_method" value="DELETE">
                                {{$address->address}} <button class="submit pull-right" style="display: inline-block">suprimer</button>
                                </form>
                            </li>
                        @endforeach
                        <button class="submit" id="new_address" style="margin-bottom: 10px">Ajouter adresse</button>
                     <form action="{{url('/address')}}" method="post">
                        <div class="form-group address-form" style="width: 467px;display: none" >
                            <input id="autocomplete" class=" form-control" placeholder="Enter your address"
                                   onFocus="geolocate()" type="text"  name="address" required>
                        </div>
                        <div style="padding: 15px;margin: 15px 0px; background:rgba(37,37,36,0.6); width: 467px; border-radius: 8px;display: none" class="address-form">

                            <table id="address">
                                <div class="form-group">
                                    <label for="" style="display: block">Street address</label>
                                    <input class="field form-control" id="street_number" disabled="true" style="width: 20%;display: inline-block" name="street_number">
                                    <input class="field form-control" id="route" disabled="true" style="width: 79%;display: inline-block" name="route">

                                </div>
                                <div class="form-group">
                                    <label for="">City</label>
                                    <input class="field form-control" id="locality" disabled="true" name="city">
                                </div>
                                <div class="form-group">
                                    <label for="">State</label>
                                    <input class="field form-control" id="administrative_area_level_1" disabled="true" name="state">
                                </div>
                                <div class="form-group">
                                    <label for="">Post code</label>
                                    <input class="field form-control" id="postal_code" disabled="true" name="post">
                                </div>
                                <div class="form-group">
                                    <label for="">Country</label>
                                    <input class="field form-control" id="country" disabled="true" name="country">
                                </div>
                                <div class="form-group" >
                                    <label for="">Intitulé de l'adresse *</label>
                                    <input type="text" name="lib_address" class=" form-control" required
                                           value="{{Auth::user()->lib_address}}">
                                </div>
                            </table>
                            <div class="form-group">
                                <button type="submit" class="submit">Valide</button>
                            </div>
                        </div>
                     </form>
                </section>
            </div>
        </div>

    </div>
    <script>
        var placeSearch, autocomplete;
        var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'long_name',
            postal_code: 'short_name'
        };

        function initAutocomplete() {
            // Create the autocomplete object, restricting the search to geographical
            // location types.
            autocomplete = new google.maps.places.Autocomplete(
                    /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                    {types: ['geocode']});

            // When the user selects an address from the dropdown, populate the address
            // fields in the form.
            autocomplete.addListener('place_changed', fillInAddress);
        }

        // [START region_fillform]
        function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();

            for (var component in componentForm) {
                document.getElementById(component).value = '';
                document.getElementById(component).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;
                }
            }
        }
        // [END region_fillform]

        // [START region_geolocation]
        // Bias the autocomplete object to the user's geographical location,
        // as supplied by the browser's 'navigator.geolocation' object.
        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var circle = new google.maps.Circle({
                        center: geolocation,
                        radius: position.coords.accuracy
                    });
                    autocomplete.setBounds(circle.getBounds());
                });
            }
        }
        $('.submit-form').click(function () {
            $(this).parent('form').submit();
        });
        $('#change').click(function(){
            $('#change_form').toggle();
        });
        $('#new_address').click(function () {
           $('.address-form').slideToggle();
        });
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIbJ48RCsE-UPzW9y-3hmxWpNVKm6tYho&language=fr&libraries=places&callback=initAutocomplete"
            type="text/javascript"></script>

@endsection