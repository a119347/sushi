@extends('app.app_template')
@section('content')
    <style>
        #content{
            min-height: 950px;
            background: #111111;
        }
        #main{
            width: 80vw;
            height:80vh;
            margin: 0 auto;
            padding-top: 9vh;
        }
        #main > p {
            font-size: 2.5rem;
            color: #BAAA76;
        }
        #checkout{
            width: 40vw;
            float: left;

        }
        #checkout,#livraison-info > p {
            font-size: 2rem;
            color: #BAAA76;

        }
        #livraison-info
        {
            width: 30vw;
            float: right;
        }
        #livraison-info > section
        {
            background: rgba(37,37,36,0.6);
            font-size: 1.5rem;
            padding: 1vw;
            color: #BAAA76;
        }
        #livraison-info > section >p:nth-of-type(2)
        {
            font-style: italic;
        }
        #menu-info
        {
            background: rgba(37,37,36,0.6);
            font-size: 1.5rem;
            color: #BAAA76;
            min-height: 250px;
            padding: 20px;
        }
        #btn-submit{
            display: block;
            width: 80px;
            background: #BAAA76;
            color: black;
            border: 0pt;
            box-shadow: none;
            border-radius: 5px;
        }
    </style>
<div id="content">
    <div id="main">
        <p>VALIDATION ET PAIEMENT</p>
        <div id="checkout">
            <p style="font-size: 14px;">RECAPITULATIF DE VOTRE COMMANDE ET PAIEMENT</p>
            <div id="menu-info">
                <ul style="border-bottom: 2px solid #BAAA76;padding:5px 0px; list-style: none">
                    @php
                        $total=0;
                    @endphp
                    @foreach($orders as $order)
                        <li>
                            <span style="font-size: 1.2em">{{$order->name}}</span>
                            <span style="padding: 0px 10px;opacity: 0.6;">{{$order->boisson}}</span>
                            <span style="padding: 0px 10px;opacity: 0.6;">{{$order->riz}}</span>
                            <span class="pull-right" >{{$order->total}}€</span>
                            <span class="pull-right" style="padding: 0vw 5vw;">x{{$order->qty}}</span>
                            @php
                                $total+=$order->total;
                            @endphp
                        </li>
                    @endforeach
                </ul>
                <ul style="list-style: none;padding:5px 0px;border-bottom: 2px solid #BAAA76">
                    @if(isset($promotion))

                        <li>
                            <span style="font-size: 1.2em">Promotion</span>
                            <span class="pull-right" style="font-size: 1.2em" >{{$promotion}}€</span>
                        </li>
                    @endif
                    <li>
                        <span style="font-size: 1.2em">TOTAL HT</span>
                        <span class="pull-right" style="font-size: 1.2em" >{{$total-$promotion}}€</span>
                    </li>
                    <li>
                        <span style="font-size: 1.2em">Taxes</span>
                        <span class="pull-right" style="font-size: 1.2em" >{{$order->total}}€</span>
                    </li>


                    <li>
                        <span style="font-size: 1.2em">Sous-total</span>
                        <span class="pull-right" style="font-size: 1.2em" >{{$order->total}}€</span>

                    </li>
                </ul>
                <ul style="list-style: none;padding: 0px">
                    <li>
                        <span style="font-size: 1.5em">TOTAL TTC</span>
                        <span class="pull-right" style="font-size: 1.2em" >{{number_format(($total-$promotion)*1.2,2)}}€</span>
                    </li>
                </ul>
            </div>
            <div style="margin-top: 20px;">
                @include('app.payment')
            </div>


        </div>
        <div id="livraison-info">
            <p>LIVRAISON</p>
            <section>
                <p>{{Auth::user()->nom}} {{Auth::user()->prenom}}</p>
                <p>Vous devrez vous deplacer dans le point relais ci-dessous pour retirer votre commande</p>
                <p>Horaire:</p>
                <p>{{$data['date']}} &nbsp; {{$data['time']}}</p>
                @if(isset($data['relais']))
                <p>Boutique</p>
                    {{$data['relais']}} <br>
                    <span style="font-style: italic">{{$data['relais_place']}}</span>
                    @else
                    <p>Votre address</p>
                    {{isset($data['autre-address'])?$data['autre-address']:$data['address']}}
                @endif
            </section>
        </div>

    </div>

</div>
    <script src="http://parsleyjs.org/dist/parsley.js"></script>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script>
        // This identifies your website in the createToken call below
        Stripe.setPublishableKey('pk_test_YYzdZX6viYDzv08OInmBXL7F');

        jQuery(function($) {
            $('#payment-form').submit(function(event) {
                var $form = $(this);

                // Before passing data to Stripe, trigger Parsley Client side validation
                $form.parsley().subscribe('parsley:form:validate', function(formInstance) {
                    formInstance.submitEvent.preventDefault();
                    return false;
                });

                // Disable the submit button to prevent repeated clicks
                $form.find('#submitBtn').prop('disabled', true);

                Stripe.card.createToken($form, stripeResponseHandler);

                // Prevent the form from submitting with the default action
                return false;
            });
        });

        function stripeResponseHandler(status, response) {
            var $form = $('#payment-form');

            if (response.error) {
                // Show the errors on the form
                $form.find('.payment-errors').text(response.error.message);
                $form.find('.payment-errors').addClass('alert alert-danger');
                $form.find('#submitBtn').prop('disabled', false);
                $('#submitBtn').button('reset');
            } else {
                // response contains id and card, which contains additional card details
                var token = response.id;
                console.log(token);
                // Insert the token into the form so it gets submitted to the server
                $form.append($('<input type="hidden" name="stripeToken" />').val(token));
                // and submit
                $form.get(0).submit();
            }
        };
    </script>
@endsection