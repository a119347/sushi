@extends('app.app_template')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <style>
        /*select {*/
            /*border: solid 1px #000;*/

            /*appearance:none;*/
            /*-moz-appearance:none;*/
            /*-webkit-appearance:none;*/

            /*padding-right: 14px;*/

            /*background: url("http://ourjs.github.io/static/2015/arrow.png") no-repeat scroll right center transparent;*/

        /*}*/


        #content
        {
            width: 80vw;
            margin: 0px auto;
            color: #BAAA76;
            top:34px;
            position: relative;
        }
        #shop-img
        {
            height: 46px;
            width: 46px;
            float: left;
        }
        #text
        {
            font-size: 38px;
            font-weight: bold;
            color: #BAAA76;
            font-family: Corbel;
            line-height: 54px;
        }
        .radio
        {
            position: relative;
            display: inline;
            margin-bottom: 20px;
        }
        .qty-info
        {
            width: 50px;
            display: inline-block;
            background: black;
            padding: 0px 1px;
            margin-right: 150px;

        }
        .list-group-item{
            padding: 35px 0px;
            background: none;
            border: none;
            border-bottom: 2px solid #BAAA76;
            height: 140px;
        }
        #livraison-info
        {
            background:rgba(37,37,36,0.6);
            position: relative;
            height: 500px;
            padding: 20px 34px;
            margin-bottom: 82px;

        }
        #show-emporter , #show-livrison
        {
            float: left;
            width: 40vw;
        }

        #map {
            height: 300px;
            width: 32vw;
            margin: 0 auto;
            top: 20px;
            position: relative;
            float: right;
        }
        /*radio css*/
        .radio [type="radio"]{
            display: none;
        }
        .radio label{
            float: left;
            display: inline-block;
            position: relative;
            width: 20px;
            height: 20px;
            border: 1px #BAAA76 solid;
            background-color: #252524;

            margin-right: 10px;
            cursor: pointer;
            border-radius: 100%;
        }
        .radio label:after{
            content: '';
            position: absolute;
            top: 7px;
            left: 7px;
            width: 5px;
            height: 5px;
            background-color: #BAAA76;
            border-radius: 50%;
            transform: scale(0);
            transition:transform .2s ease-out;
        }
        .radio [type="radio"]:checked + label{
            background-color: #252524;
            transition:background-color .2s ease-in;
        }
        .radio [type="radio"]:checked + label:after{
            transform:scale(1);
            transition:transform .2s ease-in;
        }
        .radio > sapn{
            display: inline-block;
            height: 20px;
            line-height: 20px;
            float: left;
        }
        /*下拉菜单*/
        .content{
            padding-top: 10px;
        }

        @-webkit-keyframes slide-down{
            0%{transform: scale(1, 0);}
            25%{transform: scale(1, 1.2);}
            50%{transform: scale(1, 0.85);}
            75%{transform: scale(1, 1.05);}
            100%{transform: scale(1, 1);}
        }
        .content .select.open ul{
            max-height: 250px;
            transform-origin:50% 0;
            -webkit-animation:slide-down .5s ease-in;
            transition: max-height .2s ease-out;
            padding: 0px;
            text-align: center;
        }
        .content .select.open:after{
            transform:rotate(-180deg);
            transform-origin:10px 0px;
            top: 18px;
            transition:all .3s ease-in;
        }
        #btn-submit{
            display: block;
            width: 80px;
            background: #BAAA76;
            color: black;
            border: 0pt;
            box-shadow: none;
            border-radius: 5px;
        }
        #btn-submit:hover{
            opacity: 0.5;
        }
        .fa{
            cursor: pointer;
        }
        /*loading动画*/
        .spinner {
            margin: 100px auto;
            width: 40px;
            height: 40px;
            position: relative;
        }

        .container1 > div, .container2 > div, .container3 > div {
            width: 6px;
            height: 6px;
            background-color: #333;

            border-radius: 100%;
            position: absolute;
            -webkit-animation: bouncedelay 1.2s infinite ease-in-out;
            animation: bouncedelay 1.2s infinite ease-in-out;
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both;
        }

        .spinner .spinner-container {
            position: absolute;
            width: 100%;
            height: 100%;
        }

        .container2 {
            -webkit-transform: rotateZ(45deg);
            transform: rotateZ(45deg);
        }

        .container3 {
            -webkit-transform: rotateZ(90deg);
            transform: rotateZ(90deg);
        }

        .circle1 { top: 0; left: 0; }
        .circle2 { top: 0; right: 0; }
        .circle3 { right: 0; bottom: 0; }
        .circle4 { left: 0; bottom: 0; }

        .container2 .circle1 {
            -webkit-animation-delay: -1.1s;
            animation-delay: -1.1s;
        }

        .container3 .circle1 {
            -webkit-animation-delay: -1.0s;
            animation-delay: -1.0s;
        }

        .container1 .circle2 {
            -webkit-animation-delay: -0.9s;
            animation-delay: -0.9s;
        }

        .container2 .circle2 {
            -webkit-animation-delay: -0.8s;
            animation-delay: -0.8s;
        }

        .container3 .circle2 {
            -webkit-animation-delay: -0.7s;
            animation-delay: -0.7s;
        }

        .container1 .circle3 {
            -webkit-animation-delay: -0.6s;
            animation-delay: -0.6s;
        }

        .container2 .circle3 {
            -webkit-animation-delay: -0.5s;
            animation-delay: -0.5s;
        }

        .container3 .circle3 {
            -webkit-animation-delay: -0.4s;
            animation-delay: -0.4s;
        }

        .container1 .circle4 {
            -webkit-animation-delay: -0.3s;
            animation-delay: -0.3s;
        }

        .container2 .circle4 {
            -webkit-animation-delay: -0.2s;
            animation-delay: -0.2s;
        }

        .container3 .circle4 {
            -webkit-animation-delay: -0.1s;
            animation-delay: -0.1s;
        }

        @-webkit-keyframes bouncedelay {
            0%, 80%, 100% { -webkit-transform: scale(0.0) }
            40% { -webkit-transform: scale(1.0) }
        }

        @keyframes bouncedelay {
            0%, 80%, 100% {
                transform: scale(0.0);
                -webkit-transform: scale(0.0);
            } 40% {
                  transform: scale(1.0);
                  -webkit-transform: scale(1.0);
              }
        }
        select.form-control,input.form-control{
            width: 15vw;
            background: #ccc;
        }
        .error
        {
            border:1px solid red;
        }
    </style>
    <div class="container-fluid" style="background: #111111;min-height: 500px;">
        <div id="content">
            <img  id="shop-img" src="/images/shopping-01.png" alt="shopping_log" >
            <span id="text">PANIER</span>
            <div id="panier-info" style="background:rgba(37,37,36,0.6);margin-top: 26px;padding: 40px  34px ">
                <p style="font-size: 18px;font-weight: bold;">RECAPITULATIF DE VOTRE PANNIER</p>
                <ul class="list-group" style="border: none" id="result">
                    <div id="loading" style="display: block;position:absolute;left: 40%; ">
                        <div class="spinner">
                            <div class="spinner-container container1">
                                <div class="circle1"></div>
                                <div class="circle2"></div>
                                <div class="circle3"></div>
                                <div class="circle4"></div>
                            </div>
                            <div class="spinner-container container2">
                                <div class="circle1"></div>
                                <div class="circle2"></div>
                                <div class="circle3"></div>
                                <div class="circle4"></div>
                            </div>
                            <div class="spinner-container container3">
                                <div class="circle1"></div>
                                <div class="circle2"></div>
                                <div class="circle3"></div>
                                <div class="circle4"></div>
                            </div>
                        </div>
                    </div>
                </ul>
                <ul class="list-group" style="border: none">
                    <li class="list-group-item" style="height: 147px;font-size: 14px;">
                        <div class="row" style="margin: 0px">
                            <p style="opacity:0.6;width: 742.5px;display: inline-block;float:left;">Nombre de produits</p>
                            <span id="product-total-count"></span>
                        </div>
                        <div class="row" style="margin: 0px;">
                            <p style="opacity:0.6;display: inline-block;float:left;width: 742.5px">Nombre de pièces</p>
                            <span id="product-total-piece" style="float:left;display: inline-block;"></span>
                        </div>
                    </li>
                    <li class="list-group-item" style="font-size: 14px">

                        <div class="row" style="margin: 0px">
                            <p style="opacity:0.6;width: 742.5px;display: inline-block;float:left; font-size: 20px;">Total</p>
                            <span id="total_price" style="font-size: 20px;">0€</span>
                        </div>
                    </li>
                </ul>
            </div>
            <p style="font-size: 33px;font-weight: bold;margin-top: 102px">Mode de livraison <i class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" data-placement="right"></i> </p>
            <div id="livraison-info" >
                    <div class="radio ">
                    <input type="radio" name="livraison-type" id="livraison"  value="livraison"/>
                    <label for="livraison"></label> <span style="float:left;"> LIVRAISON</span>

                    <input type="radio" name="livraison-type" id="emporter" checked="checked" value="emporter"/>
                    <label for="emporter" style="margin-left: 10px"></label> <span style="float:left;">A EMPORTER</span>
                </div>
                <br>
                <div id="show-livrison">
                    <form action="{{url('/checkout')}}" method="post" id="checkout">
                        {{csrf_field()}}
                        <input type="hidden" name="livraison-type" value="livraison">
                    <div class="content">
                        <p>Votre address:</p>
                        <div class="form-group" >
                            <input type="text" id="start_place" placeholder="saisir votre adresse" style="display: block;" class="form-control">
                        </div>

                        <p>Selectionnez un point relais</p>
                        <div class="form-group">
                            <select name="relais" id="select-place" class="form-control" style="width: 15vw"  required>
                                <option value="" disabled>Selectionnez un point relais</option>
                                @inject('points','App\Relais')
                                <option value="all" data-place="all" >Tout de boutique</option>
                                @foreach($points::all() as $key=>$point)
                                    <option data-place="{{$point->address}}" data-index="{{$key+1}}" data-value="{{$point->name}}" data-id="{{$point->id}}" value="{{$point->name}}">{{$point->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <input type="hidden" name="relais_place" id="relais_place">
                        <p>Date:</p>
                        <div class="form-group">
                            <select name="date" id="select-date" class="form-control" required>
                                <option value="{{date('d-m-Y')}}">Aujourd'hui</option>
                                <option value="" disabled>Autre</option>
                                <option value="{{date('d-m-Y',strtotime('+1 day'))}}">{{date('d-m-Y',strtotime('+1 day'))}}</option>
                                <option value="{{date('d-m-Y',strtotime('+2 day'))}}">{{date('d-m-Y',strtotime('+2 day'))}}</option>
                            </select>
                        </div>
                        <p>Time:</p>
                        <div class="form-group">
                            <select name="time" id="select-time" class="form-control" required>
                                <option value="12:00-12:30">12:00-12:30</option>
                            </select>
                        </div>
                    </div>
                        @if (Auth::guest())
                            <a id="btn-submit" class="btn" style="position: relative;top: 30px;display: block;clear:both;" href="{{url('/connexion')}}">VALIDER</a>
                        @elseif(cart::total()==0)
                            <button id="btn-submit" class="btn " style="position: relative;top: 30px;display: block;cursor:not-allowed;clear:both;" >VALIDER</button>
                        @else
                            <button id="btn-submit" class="btn liv-btn " style="position: relative;top: 30px;display: block;clear:both;" >VALIDER</button>

                        @endif
                    </form>
                </div>
                <div id="show-emporter" style="display: none">
                    <form action="{{url('/checkout')}}" method="post">
                        <input type="hidden" name="livraison-type" value="emporter">
                        {{csrf_field()}}

                        <div class="content">
                            <p>Votre address:</p>
                            <div class="form-group">
                                <select name="address" id="address" class="form-control" required>
                                    @if(isset(Auth::user()->address))
                                    <option value="{{Auth::user()->address}}">{{Auth::user()->address}}</option>
                                    @endif
                                    <option value="autre">Autre</option>
                                </select>
                            </div>
                            <div class="form-group" style="display: {{isset(Auth::user()->address)?'none':''}}" id="autre-address">

                                <input type="text" class="form-control" id="start_place2" name="autre-address" placeholder="saisir votre adresse" {{isset(Auth::user()->address)?'':'required'}}>
                            </div>
                            <p>Date:</p>
                            @php
                            $time =strtotime('11:00:00');
                            $end=strtotime(date('H:i:s'));
                            @endphp
                            <div class="form-group">
                                <select name="date" id="" class="form-control" required>
                                    @if($end<$time)
                                        <option value="{{date('d-m-Y')}}">Aujourd'hui</option>
                                    @endif
                                    <option value="autre" disabled>Autre</option>
                                    <option value="{{date('d-m-Y',strtotime('+1 day'))}}">{{date('d-m-Y',strtotime('+1 day'))}}</option>
                                    <option value="{{date('d-m-Y',strtotime('+2 day'))}}">{{date('d-m-Y',strtotime('+2 day'))}}</option>
                                </select>
                            </div>
                            <p>Time:</p>
                            <div class="form-group">
                                <select name="time" id="emporter-today-time" class="form-control" required>
                                    <option value="12:00-12:30">12:00-12:30</option>
                                </select>
                            </div>

                            <input type="hidden" id="lat" name="lat">
                            <input type="hidden" id="lng" name="lng">
                            @inject('cart','Overtrue\LaravelShoppingCart\Cart')
                    </div>
                        {{--11点之前只能点lunch sushi的menu--}}
                        @if (Auth::guest())
                            <a id="btn-submit" class="btn" style="position: relative;top: 30px;display: block;clear:both;" href="{{url('/connexion')}}">VALIDER</a>
                        @elseif(cart::total()==0)
                            <button id="btn-submit" class="btn " style="position: relative;top: 30px;display: block;cursor:not-allowed;clear:both;" >VALIDER</button>
                        @else
                            <button id="btn-submit" class="btn " style="position: relative;top: 30px;display: block;clear:both;" >VALIDER</button>

                        {{--@foreach(cart::all() as $item)--}}
                                {{--@if($item->cat!=9)--}}
                                    {{--<span style="font-style: italic;color: red">中午只能点menu midi</span>--}}
                                    {{--<button id="btn-submit" class="btn " style="position: relative;top: 30px;display: block;clear:both;cursor: not-allowed;" onclick="return false;" >VALIDER</button>--}}
                                    {{--@break--}}
                                    {{--@else--}}
                                    {{--<button id="btn-submit" class="btn " style="position: relative;top: 30px;display: block;clear:both;" >VALIDER</button>--}}

                                {{--@endif--}}

                            {{--@endforeach--}}

                        @endif
                </form>

                </div>

                <div id="map">
                </div>

                {{--没登录--}}

            </div>
        </div>
    </div>
    <script src="/js/cart.js"></script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIbJ48RCsE-UPzW9y-3hmxWpNVKm6tYho&language=fr&libraries=places" type="text/javascript"></script>
    <script src="/js/panier.js"></script>
    <script>
        if($( "#select-place option:selected" ).val()=='all')
        {
            $('.liv-btn').click(function () {
                $('#select-place').addClass('error');
                return false;
            });
        }
        getCart();
        $("[data-toggle='tooltip']").tooltip();
        function initMap() {
            //获取用户地址
            var input =document.getElementById('start_place');
            var input2 =document.getElementById('start_place2');

            var directionsService = new google.maps.DirectionsService();
            var directionsDisplay = new google.maps.DirectionsRenderer();

            //map style
            var styles = [
                {
                    stylers: [
                        { hue: 340 },
                        { saturation: 5.6 },
                        { lightness: 21.2 },
                        { gamma: 1.51 }
                    ]
                },{
                    featureType: "road",
                    elementType: "geometry",
                    stylers: [
                        { hue: "#00ffee" },
                        { lightness: 100 },
                        { visibility: "simplified" }
                    ]
                },{
                    featureType: "road",
                    elementType: "labels",
                    stylers: [
                        { visibility: "off" }
                    ]
                }
            ];
            var styledMap = new google.maps.StyledMapType(styles,
                    {name: "Styled Map"});
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 9,
                center: {lat: 48.8588377, lng: 2.2775171},
                mapTypeControlOptions: {
                    mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
                }
            });
            map.mapTypes.set('map_style', styledMap);
            map.setMapTypeId('map_style');
            var geocoder = new google.maps.Geocoder();

            var markers = [];
            var autocomplete = new google.maps.places.Autocomplete(input);
            var autocomplete2 = new google.maps.places.Autocomplete(input2);
//            var image = '/images/position-icon.png';
            var image = {
                url: '/images/position-icon.png',
                size: new google.maps.Size(71, 71),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };


            function geocodeAddress(address,geocoder, resultsMap,index) {
                var address = address;
                geocoder.geocode({'address': address}, function(results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        resultsMap.setCenter(results[0].geometry.location);
//                        var marker = new google.maps.Marker({
//                            map: resultsMap,
//                            icon: image,
//                            position: results[0].geometry.location
//                        });
                        var content="<div style='color: #BAAA76'>"+address+"</div>";
                        addMarker(results[0].geometry.location,index,content)
                    } else {
                        alert('Geocode was not successful for the following reason: ' + status);
                    }
                });
            }
            var infowindow = new google.maps.InfoWindow();

            function attachSecretMessage(marker, secretMessage) {
               infowindow.close();
                infowindow.setContent(secretMessage);
                marker.addListener('click', function() {
                    infowindow.close(marker.get('map'), marker);
                    map.setZoom(10);
                    infowindow.open(map, marker);
                });
            }
            var marker1 = new google.maps.Marker({
                map: map,
                anchorPoint: new google.maps.Point(0, -29)
            });
            autocomplete.addListener('place_changed', function() {

                calcRoute();
                infowindow.close();
                marker1.setVisible(false);
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                    window.alert("Autocomplete's returned place contains no geometry");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(17);  // Why 17? Because it looks good.
                }
                marker1.setIcon(/** @type {google.maps.Icon} */({
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(35, 35)
                }));
                marker1.setPosition(place.geometry.location);
                marker1.setVisible(true);

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                    ].join(' ');
                }

                infowindow.setContent('<div>Votre place<strong>' + place.name + '</strong><br>' + address);
                infowindow.open(map, marker1);
            });
            autocomplete2.addListener('place_changed', function() {
                infowindow.close();
                marker1.setVisible(false);
                var place2 = autocomplete2.getPlace();
                if (!place2.geometry) {
                    window.alert("Autocomplete's returned place contains no geometry");
                    return;
                }

                // If the place has a geometry, then present it on a map.
                if (place2.geometry.viewport) {
                    map.fitBounds(place2.geometry.viewport);
                } else {
                    map.setCenter(place2.geometry.location);
                    map.setZoom(17);  // Why 17? Because it looks good.
                }
                marker1.setIcon(/** @type {google.maps.Icon} */({
                    url: place2.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(35, 35)
                }));
                marker1.setPosition(place2.geometry.location);
                marker1.setVisible(true);

                var address = '';
                if (place2.address_components) {
                    address = [
                        (place2.address_components[0] && place2.address_components[0].short_name || ''),
                        (place2.address_components[1] && place2.address_components[1].short_name || ''),
                        (place2.address_components[2] && place2.address_components[2].short_name || '')
                    ].join(' ');
                }
                $('#lat').val(place2.geometry.location.lat());
                $('#lng').val(place2.geometry.location.lng());

                infowindow.setContent('<div>Votre place<strong>' + place2.name + '</strong><br>' + address);
                infowindow.open(map, marker1);
            });
            function addMarker(position,index,content){
                var marker =new google.maps.Marker({
                    position: position,
                    map: map,
//                        icon: image,
                    animation: google.maps.Animation.DROP,
                    label:(index+1)+''
                });
                attachSecretMessage(marker, content);
                markers.push(marker)
            }

            function calcRoute() {
                var start = input.value;
                var end = $('#end_place').attr('data-place');
                var request = {
                    origin: start,
                    destination: end,
                    travelMode: google.maps.TravelMode.DRIVING
                };
                directionsService.route(request, function (response, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        directionsDisplay.setDirections(response);
                        var route = response.routes[0];
                        var distance;
                        var summaryPanel = document.getElementById('distance');
                        distance=route.legs[0].distance.text;
                        $('#distance').html(distance);
                    } else {
//                        alert('请输入出发地和到达地');
                    }
                });
            }

            function setMapOnAll(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            function clearMarkers() {
                setMapOnAll(null);
            }
            function deleteMarkers() {
                clearMarkers();
                markers = [];
            }
            $.get('/json', function (data) {
                $.each(data, function (key,val) {
                    geocodeAddress(val.address,geocoder, map,key);
                })
            });
            //获得该地点营业时间
            function getTime(id){
                $.get('/timeJson',{id:id}, function (response) {
                    var html='<option value="'+response+'">'+response+'</option>';
                    $('#select-time').html(html);
                })
            }
            $('#select-place').change( function(e){
                deleteMarkers();
                var _this = $( "#select-place option:selected" );
                var place=_this.attr('data-place');
                var index=_this.attr('data-index');
                var id=_this.attr('data-id');

                if (place=='all'){
                    $.get('/json', function (data) {
                        $.each(data, function (key,val) {
                            geocodeAddress(val.address,geocoder, map,key);
                        })
                    });

                }else {
                    getTime(id);
                    geocodeAddress(place,geocoder,map,index);
                    calcRoute();
                    $('.liv-btn').click(function () {
                        $('#checkout').submit();
                    });
                    $('#relais_place').val(place)
                }
                $('#end_place').attr('data-place',place);
                e.stopPropagation();
            });
        }
        initMap();
        $('#address').change(function () {
            console.log($( "#address option:selected" ).val());
            if ($( "#address option:selected" ).val()=='autre')
            {
                $('#autre-address').attr('display','block');
            }else{
                $('#autre-address').attr('display','none');
            }
        });

        $('#livraison').click(function () {
            $('#show-emporter').show();
            $('#show-livrison').hide();
            $('#livraison-info').height(400+'px');

        });
        $('#emporter').click(function () {

            $('#show-livrison').show();
            $('#show-emporter').hide();
            $('#livraison-info').height(460+'px');
        });

    </script>
@endsection