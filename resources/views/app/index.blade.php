@extends('app.app_template')
@section('content')
    <style>

        html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video {
            margin: 0;
            padding: 0;
            border: 0;
            font-size: 100%;
            font: inherit;
            vertical-align: baseline
        }



        ol, ul {
            list-style: none
        }

        table {
            border-collapse: collapse;
            border-spacing: 0
        }

        caption, th, td {
            text-align: left;
            font-weight: normal;
            vertical-align: middle
        }

        q, blockquote {
            quotes: none
        }

        q:before, q:after, blockquote:before, blockquote:after {
            content: "";
            content: none
        }

        a img {
            border: none
        }

        article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section {
            display: block
        }

        article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section {
            display: block
        }

        * {
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -ms-box-sizing: border-box;
            box-sizing: border-box
        }

        a {
            color: #CCC;
            text-decoration: none
        }



        body section.container div#slider {
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            -o-border-radius: 3px;
            -ms-border-radius: 3px;
            -khtml-border-radius: 3px;
            border-radius: 3px;
            -moz-box-sizing: content-box;
            -webkit-box-sizing: content-box;
            -ms-box-sizing: content-box;
            box-sizing: content-box;
        }

        body section.container div#slider div.fluxslider div.surface {
            width: 100%;
            -moz-box-sizing: content-box;
            -webkit-box-sizing: content-box;
            -ms-box-sizing: content-box;
            box-sizing: content-box
        }

        body section.container div#slider div.fluxslider ul.pagination {
            -moz-box-sizing: content-box;
            -webkit-box-sizing: content-box;
            -ms-box-sizing: content-box;
            box-sizing: content-box;
            padding: 10px 0 !important;
            overflow: hidden
        }

        body section.container div#slider div.fluxslider ul.pagination li {
            text-indent: 10000px;
            height: 8px;
            width: 8px;
            -moz-border-radius: 10px;
            -webkit-border-radius: 10px;
            -o-border-radius: 10px;
            -ms-border-radius: 10px;
            -khtml-border-radius: 10px;
            border-radius: 10px;
            background: rgba(0, 0, 0, 0.6);
            -webkit-box-shadow: 0 1px 0 rgba(255, 255, 255, 0.1)
        }



        footer {
            width: 100%;
            text-align: center;
            font-size: 0.7em;
            margin-top: 4em;
            color: #666
        }

        footer p {
            margin-bottom: 2em
        }

        body#transitiongallery section.container {
            width: 100%
        }






        body#transitiongallery section.container div#slidercontainer div#transitions ul {
            margin-bottom: 1.5em
        }

        body#transitiongallery section.container div#slidercontainer div#transitions ul li {
            font-size: 0.8em;
            margin-bottom: 0.7em
        }




    </style>
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators" style="right: 40px">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            <li data-target="#carousel-example-generic" data-slide-to="3"></li>

        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="/images/show1.jpg" alt="...">
                <div class="carousel-caption">
                </div>

            </div>
            <div class="item">
                <img src="/images/index1.PNG" alt="...">
                <div class="carousel-caption">
                </div>
            </div>
            <div class="item">
                <img src="/images/index2.PNG" alt="...">
                <div class="carousel-caption">
                </div>
            </div>
            <div class="item">
                <img src="/images/index3.PNG" alt="...">
                <div class="carousel-caption">
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        <div  id="commandBtn"></div>
        <div id="line">
        </div>
        <div id="livraison">
            <div style="left: 190px;float:left;" class="livraison-icon">
                <i class="fa fa-home" aria-hidden="true"></i> LIVRAISON A DOMICILE
            </div>
            <div style="right: 118px;float: right;" class="livraison-icon">
                <i class="fa fa-map-marker" aria-hidden="true"></i>LIVRAISON A POINT RELAIS
            </div>
        </div>
    </div>

    {{--<section class="container" style="width: 100%;padding: 0;margin: 0">--}}

        {{--<div id="slider" style="width: 100%">--}}
            {{--<img src="/images/show1.jpg" alt="" />--}}
            {{--<img src="/images/index1.PNG" alt="" />--}}
            {{--<img src="/images/index2.PNG" alt="" />--}}
            {{--<img src="/images/index3.PNG" alt="" />--}}
        {{--</div>--}}

    {{--</section>--}}

    <script src="js/flux.min.js" type="text/javascript" charset="utf-8"></script>
    {{--<script type="text/javascript" charset="utf-8">--}}
        {{--$(function(){--}}
            {{--if(!flux.browser.supportsTransitions)--}}
                {{--alert("Flux Slider requires a browser that supports CSS3 transitions");--}}
            {{--jQuery(function ($) {--}}
                {{--window.f = new flux.slider('#slider', {--}}
                    {{--pagination: true,--}}
                    {{--autoplay: false--}}
                {{--});--}}
                {{--window.setTimeout(function () {--}}
                    {{--var surface = $('#slider .surface');--}}
                    {{--var factor = $(window).width() / surface.width();--}}
                    {{--$('#slider').css('transform', 'scale(' + factor + ', ' + factor + ')')--}}
                    {{--console.log(factor);--}}
                {{--}, 500);--}}

            {{--});--}}
        {{--});--}}
    {{--</script>--}}
    <script>
        $('#commandBtn').click(function () {
            window.location.href='/menus'
        })
    </script>
@endsection