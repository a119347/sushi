@extends('admin.admin_template')
@section('content')
    <table class="table table-hover">
    	<thead>
    		<tr>
    			<th>编号</th>
                <th>facteur</th>
                <th>用户</th>
                <th>Relais</th>
                <th>货物</th>
                <th>金额</th>
                <th>创建时间</th>
                <th>查看</th>
    		</tr>
    	</thead>
    	<tbody>
    		@foreach($orders as $key=>$order)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$order->facture}}</td>
                    <td>{{$order->belongsToUser->nom}} {{$order->belongsToUser->prenom}}</td>
                    <td>{{$order->relais}} <br>
                        {{$order->address}}
                    </td>
                    <td>
                        <ol>
                            @foreach($order->OrderContent as $content)

                            <li>
                                {{$content->product_name}}<br>
                                {{$content->boission}} <br>
                                {{$content->riz}} <br>
                                数量:{{$content->qty}}
                            </li>
                            @endforeach
                        </ol>

                    </td>
                    <td>{{$order->price}}</td>
                    <td>
                        {{$order->created_at}} <br>
                        {{$order->created_at->diffForHumans()}}
                    </td>
                    <td><a href="{{url('/admin/orders/'.$order->id)}}" class="btn btn-success">修改</a></td>
                </tr>
            @endforeach
    	</tbody>
    </table>
    {!! $orders->render() !!}
@endsection