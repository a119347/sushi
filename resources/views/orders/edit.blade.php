@extends('admin.admin_template')
@section('content')
    <table class="table table-hover">
        <tbody>
    		<tr>
    			<td>facteur</td>
                <td>{{$order->facture}}</td>
    		</tr>
        <tr>
            <td>收货人</td>
            <td>{{$order->belongsToUser->nom}} {{$order->belongsToUser->prenom}}</td>
        </tr>
        <tr>
            <td>Poin Relais</td>
            <td>{{$order->relais}}</td>
        </tr>
        <tr>
            <td>货物</td>
            <td>
                <ol>
                    @foreach($contents as $content)
                    <li>
                        <form action="{{url('/admin/order/'.$order->id.'/content/'.$content->id)}}" method="post">
                            {!! csrf_field() !!}
                            <input type="hidden" name="orderid" value="{{$order->id}}">
                            <input type="hidden" name="contentid" value="{{$content->id}}">
                            menu: <input type="text" value="{{$content->product_name}}" name="product_name">
                            qty:<input type="number" value="{{$content->qty}}" name="qty">
                            price: <input type="number" value="{{$content->price}}" name="price">
                            <button class="btn btn-default" onclick="return confirm('确认修改')" formaction="{{url('/admin/order/'.$order->id.'/content/'.$content->id)}}">修改</button>
                        </form>
                    </li>
                    @endforeach
                </ol>
            </td>
        </tr>
        <tr>
            <td>总金额</td>
            <td>{{$order->price}}</td>
        </tr>
    	</tbody>
    </table>
    <a href="{{url('/admin/order/livraison/')}}" class="btn btn-default">返回</a>
@endsection