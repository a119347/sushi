@extends('admin.admin_template')
@section('content')
    <style>
        canvas{
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
    <script src="/js/Chart.bundle.js"></script>
    <div style="width: 75%">
        <h4>每日订单数量</h4>
        <input type="date" name="date" id="date">
        <canvas id="chart"></canvas>
    </div>
    <div style="width: 75%">
        <h4>每月订单数量</h4>
        <canvas id="myChart"></canvas>
    </div>
    <script>
        var date=new Date();
        console.log(date);
        if (date.getMonth()<10)
        {
            var month='0'+(date.getMonth()+1);
        }else {
            var month=date.getMonth()+1;

        }
        var today=date.getFullYear()+'-'+month+'-'+date.getDate();
        {{--每日订单数量--}}
        $.get('/daycount',{date:today},function (res) {
            config.data.datasets[0].data[0]=res[0].liv_qty;
            config.data.datasets[0].data[1]=res[0].emp_qty;
            window.myChart.update();
        });
        var config={
            type: 'doughnut',
            data: {
                labels: ["Livraison", "emporter"],
                datasets: [{
                    label: 'Livraison',
                    data: [1, 2],
                    backgroundColor: [
                        "#FF6384",
                        "#36A2EB"
                    ]
                }]
            }
        };
        window.onload = function() {
            var ctx = document.getElementById("chart");
            window.myChart = new Chart(ctx,config);
            var ctx1 = document.getElementById("myChart");
            var myChart1 = new Chart(ctx1,config1);
        };
        $('#date').change(function () {
            var date=$('#date').val();
            $.get('/daycount',{date:date},function (res) {
                config.data.datasets[0].data[0]=res[0].liv_qty;
                config.data.datasets[0].data[1]=res[0].emp_qty;
                window.myChart.update();

            });
        });

        var config1={
            type: 'line',
            data: {
                labels: ["1月", "2月", "3月", "4月", "5月", "6月", "7月","8月","9月","10月","11月","12月"],
                datasets: [{
                    label: 'Livraison',
                    data:<?php echo $liv_month_qty;?>,
                    backgroundColor: 'rgba(255, 99, 132, 0.2)',
                    borderColor: 'rgba(255,99,132,1)',
                    borderWidth: 1
                },{
                    label:'emporter',
                    data: <?php echo  $emp_month_qty;?>,
                    backgroundColor: 'rgba(255, 199, 132, 0.2)',
                    borderColor: 'rgba(255,199,132,1)',
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                },
                hover: {
                    mode: 'dataset'
                }
            }
        };

    </script>
@endsection