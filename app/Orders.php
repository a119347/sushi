<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class Orders extends Model
{
    //
    protected $table='orders';
    protected $fillable=['user_id','menus','price','address','zip_code'];

    public function showUser($userId)
    {

        return DB::table('users')->where('id',$userId)->first()->nom;
    }

    public function countOrder()
    {
        return count(Orders::where('created_at','like','%'.date('Y-m-d').'%')->get());
    }

    public function belongsToUser()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function OrderContent()
    {
        return $this->hasMany(OrderContent::class,'order_id');
    }

    public function liv_qty()
    {
        return count(Orders::where('created_at','like','%'.date('Y-m-d').'%')
            ->where('type','livraison')
            ->get());
    }
    //每月的数量
    public function liv_month_qty($month)
    {
        return count(Orders::where('created_at','like','%'.$month.'%')
            ->where('type','livraison')
            ->get());
    }
    public function emp_qyt()
    {
        return count(Orders::where('created_at','like','%'.date('Y-m-d').'%')
            ->where('type','emporter')
                ->get());
    }

    public function emp_month_qty($month)
    {
        return count(Orders::where('created_at','like','%'.$month.'%')
            ->where('type','emporter')
            ->get());
    }
}
