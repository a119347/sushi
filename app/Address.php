<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    //
    protected $table='address';
    protected $fillable=['user_id','address','street_number','route','state','post','city','country','lib_address'];
}
