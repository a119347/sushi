<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderContent extends Model
{
    //
    protected $table='OrderContent';
    protected $fillable=['order_id','productid','product_name','boission','riz','qty','price'];
}
