<?php

namespace App\Http\Controllers;

use App\Orders;
use App\Relais;
use Illuminate\Http\Request;

use App\Http\Requests;

class JsonController extends Controller
{
    //
    public function getTime(Request $request)
    {
        $id=$request->get('id');
        return Relais::findOrFail($id)->send_time;
    }
    
    //查询每日liv和emp的数据
    public function getCount(Request $request)
    {
        $date=$request->get('date');
        $liv_qty= count(Orders::where('created_at','like','%'.$date.'%')
            ->where('type','livraison')
            ->get());
        $emp_qty=count(Orders::where('created_at','like','%'.$date.'%')
            ->where('type','emporter')
            ->get());
        return array(['liv_qty'=>$liv_qty,'emp_qty'=>$emp_qty]);
    }

    public function getMonthCount(Request $request)
    {
        $month=$request->get('month');
        $liv_qty= count(Orders::where('created_at','like','%'.$month.'%')
            ->where('type','livraison')
            ->get());
        $emp_qty=count(Orders::where('created_at','like','%'.$month.'%')
            ->where('type','emporter')
            ->get());
        return array(['liv_qty'=>$liv_qty,'emp_qty'=>$emp_qty]);
    }
}
