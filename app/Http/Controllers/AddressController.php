<?php

namespace App\Http\Controllers;

use App\Address;
use Illuminate\Http\Request;

use App\Http\Requests;

class AddressController extends Controller
{
    //
    public function destroy($id)
    {
        Address::destroy($id);
        return redirect()->back();
    }
}
