<?php

namespace App\Http\Controllers;

use App\OrderContent;
use App\Orders;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\App;

class PdfController extends Controller
{
    //
    public function create(Request $request)
    {
        $orders=OrderContent::where('order_id',$request->get('id'))->get();
        $facture=Orders::findOrFail($request->get('id'))->facture;
        $pdf = PDF::loadView('pdf',compact('orders','facture'));
        return $pdf->stream('invoice.pdf');
    }
}
