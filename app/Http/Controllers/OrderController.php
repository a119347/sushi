<?php

namespace App\Http\Controllers;

use App\OrderContent;
use App\Orders;
use App\Product;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Error\Card;
use Stripe\Order;
use Stripe\Stripe;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('web');
    }
    
    public function store(Request $request)
    {
         $productId=$request->get('productId');
        $boissonId=$request->get('boissonId');
        $riz=$request->get('riz');
        $product=Product::findOrFail($productId);
        $cat=$product->cat_id;
        $boisson=Product::findOrFail($boissonId);
        $price=$product->price;
        return Cart::add($productId,$product->name,1,$price,['boisson'=>$boisson->name,'piece'=>$product->count,'riz'=>$riz,'cat'=>$cat,'img'=>$product->productImage]);
    }
    //查看panier数据
    public function panier()
    {
        $carts = Cart::all();
        $count=Cart::count();
        $piece=0;
        $price=0;
        foreach ($carts as $cart) {
            $piece+=$cart->piece;
            $price+=$cart->price;
        }

        return view('app.panier',compact('carts','piece','count','price'));
    }
    //删除
    public function destroy(Request $request)
    {
        $rawId=$request->get('rawid');
        if(Cart::remove($rawId))
        {
            return  Cart::all();
        };
    }
    public function toJson()
    {
        $carts = Cart::all();
//        $carts=Cart::all();
        return $carts;
    }

    public function update(Request $request)
    {
        if($request->get('type')=='add'){
            $rawid=$request->get('rawid');
            $count=$request->get('count')+1;
            Cart::update($rawid,['qty'=>$count]);
        }else{
            $rawid=$request->get('rawid');
            $count=$request->get('count')-1;
            $price=Cart::get($rawid)->price;
            Cart::update($rawid,['qty'=>$count]);
        }
        return  Cart::all();
    }

    public function saveOrder(Request $request)
    {


        Cart::destroy();
        return redirect()->back();
    }

    public function saveorderinf(Request $request)
    {
        $liv_type=$request->get('liv_tpye');
        $address=$request->get('address');
        session('liv_type',$liv_type);
        session('address',$address);
        if ($liv_type=='')
        return redirect()->action('FrontController@checkout');
    }
    public function getDistance($lat1,$lng1,$lat2,$lng2)
    {
        $earthRadius = 6367000;
        $lat1 = ($lat1 * pi() ) / 180;
        $lng1 = ($lng1 * pi() ) / 180;
        $lat2 = ($lat2 * pi() ) / 180;
        $lng2 = ($lng2 * pi() ) / 180;
        $calcLongitude = $lng2 - $lng1;
        $calcLatitude = $lat2 - $lat1;
        $stepOne = pow(sin($calcLatitude / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($calcLongitude / 2), 2);
        $stepTwo = 2 * asin(min(1, sqrt($stepOne)));
        $calculatedDistance = $earthRadius * $stepTwo;
        return round($calculatedDistance);
    }

    public function points(Request $request){
        $lat1=$request->get('lat');
        $lng1=$request->get('lng');
        $orders=array();
        //计算和前面的订单的距离,小于100米的push到新的数组,记录total_menu;
        foreach (Orders::all() as $order) {
            $total_menu=0;
            $lat2=$order->lat;
            $lng2=$order->lng;
            $distance=$this->getDistance($lat1,$lng1,$lat2,$lng2);
            if($distance<0.1){
                array_push($orders,$order);
                if($total_menu>5){
                    $total_menu=5;
                }else{
                    $total_menu+=$order->qty;
                }
            }
        }
        //foreach循环输出小于100米的订单数组
        foreach ($orders as $item) {
            $pro_price=$item->pro_price;
            $point=($total_menu-1)*$item->qty;
            $user=User::findOrFali($item->user_id)->first();
            $user->point=$point-$pro_price;
        }
    }

    //付款
    public function payment(Request $request)
    {

        $amount=Cart::totalPrice()*100;
        $products=Cart::all();
        $menus_id=[];
        $menus_name=[];
        foreach ($products as $product)
        {
              array_push($menus_id,$product->id);
              array_push($menus_name,$product->name);
        }

        $token = $request->input('stripeToken');

        Stripe::setApiKey('sk_test_QD9r7RPD0PtSMbyt3WuHOFbJ');
//        if (!isset($emailCheck)) {
            // Create a new Stripe customer
            try {
                $customer = Customer::create([
                    'source' => $token,
                    'email' => Auth::user()->email,
                    'metadata' => [
                        "First Name" => Auth::user()->nom,
                        "Last Name" => Auth::user()->prenom
                    ]
                ]);
            } catch (Card $e) {
                return redirect()->route('order')
                    ->withErrors($e->getMessage())
                    ->withInput();
            }

            $customerID = $customer->id;
            // Create a new user in the database with Stripe
//            $user = User::create([
//                'first_name' => Auth::user()->nom,
//                'last_name' => Auth::user()->prenom,
//                'email' => Auth::user()->email,
//                'stripe_customer_id' => $customerID,
//            ]);
//        } else {
//            $customerID = User::where('email', $email)->value('stripe_customer_id');
//            $user = User::where('email', $email)->first();
//        }


        try {
            $charge = Charge::create([
                'amount' => $amount,
                'currency' => 'eur',
                'customer' => $customerID,
                'metadata' => [
                    'product_name' =>implode(',',$menus_name)
                ]
            ]);
        } catch (Card $e) {
            return redirect()->route('order')
                ->withErrors($e->getMessage())
                ->withInput();
        }
        //把数据先存入Order数据库
        $order=new Orders();
        $order->user_id=Auth::user()->id;
        $order->type=$request->get('type');
        $order->address=$request->get('address');
        $order->stripeToken=$request->get('stripeToken');
        $order->relais=$request->get('relais');
        $order->price=$request->get('price');
        $count=$order->countOrder()+1;
        $order->facture=intval(date('Ymd').'0000')+$count;
        $order->save();
        $order_id=$order->id;
//        //把购物车内容存进order数据库
        $carts=Cart::all();
        $orders=array();
        foreach ($carts as $cart) {
            array_push($orders,[
                'order_id'=>$order_id,
                'product_id'=>$cart->id,
                'product_name'=>$cart->name,
                'qty'=>$cart->qty,
                'riz'=>$cart->riz,
                'boission'=>$cart->boisson,
                'price'=>$cart->total,
                'created_at'=>Carbon::now()
            ]);
        }
        DB::table('OrderContent')->insert($orders);
        Cart::destroy();
        return redirect('/compte');
    }
    //显示livraison的订单
    public function getLiv($date=null)
    {
        if (!isset($date))
        {
            $orders=Orders::where('type','=','livraison')->orderBy('created_at','desc')->paginate(15);
        }else{
            $orders=Orders::where('type','=','livraison')->where('created_at','like','%'.$date.'%')->orderBy('created_at','desc')->paginate(15);

        }
        return view('orders.view',compact('orders'));
    }
//显示emporter的订单
    public function getEmp($date=null)
    {
        if (!isset($date))
        {
            $orders=Orders::where('type','=','emporter')->orderBy('created_at','desc')->paginate(15);
        }else{
            $orders=Orders::where('type','=','emporter')->where('created_at','like','%'.$date.'%')->orderBy('created_at','desc')->paginate(15);

        }
        return view('orders.view',compact('orders'));
    }
    public function edit($id)
    {
        $order=Orders::findOrFail($id);
        $contents=OrderContent::where('order_id',$id)->get();
        return view('orders.edit',compact('order','contents'));
    }

    public function editContent(Request $request)
    {

        OrderContent::where('id',$request->get('contentid'))->update([
            'product_name'=>$request->get('product_name'),
            'qty'=>$request->get('qty'),
            'price'=>$request->get('price')
        ]);
        //更改总结
        $contents=OrderContent::where('order_id',$request->get('orderid'))->get();
        $total=0;
        foreach ( $contents as $content) {
            $total+=$content->price;
        }
        Orders::where('id',$request->get('orderid'))->update(['price'=>$total]);
        return redirect()->back();
    }
    
}
