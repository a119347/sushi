<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\OrderContent;
use App\Orders;
use App\Product;
use App\Relais;
use App\User;
use Carbon\Carbon;

$factory->define(App\User::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('fr_FR');
    return [
        'nom' => $faker->firstName,
        'prenom'=>$faker->lastName,
        'email' => $faker->safeEmail,
        'tel'=>$faker->phoneNumber,
        'address'=>$faker->address,
        'sex'=>$faker->title,
        'societe'=>$faker->company,
        'zip_code'=>$faker->postcode,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});
$factory->define(App\Product::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'price' => $faker->numberBetween(10,20),
        'count'=>$faker->numberBetween(5,10),
        'content'=>join("\n\n",$faker->paragraphs(mt_rand(3,6))),
        'productImage'=>$faker->imageUrl(255,255),
    ];
});
$factory->define(App\Relais::class,function (Faker\Generator $faker){
    $faker = Faker\Factory::create('fr_FR');
    return [
        'name'=>$faker->company,
        'address'=>$faker->address,
        'intro'=>$faker->text($maxNbChars = 200) ,
        'send_time'=>'11:00-12:00',
    ];
});
$factory->define(App\News::class,function (Faker\Generator $faker){
    $faker = Faker\Factory::create('fr_FR');
    return [
        'title'=>$faker->company,
        'content'=>$faker->text($maxNbChars = 200),
    ];
});
$factory->define(Orders::class,function (Faker\Generator $faker){
    $faker = Faker\Factory::create('fr_Fr');
    $users=User::lists('id')->all();
    $relais=Relais::lists('id')->all();
    return [
        'user_id'=>$faker->randomElement($users),
        'price'=>$faker->numberBetween(20,200),
        'type'=>$faker->randomElement(array('emporter','livraison')),
        'created_at'=>$faker->dateTimeBetween('-2 day','now'),
        'facture'=>$faker->numberBetween(201606300001,201606301000),
        'relais_id'=>$faker->randomElement($relais)
    ];
});
$factory->define(OrderContent::class,function (Faker\Generator $faker){
    $faker = Faker\Factory::create('fr_Fr');
    $orders=Orders::lists('id')->all();
    $products=Product::lists('name')->all();
    $prices=Product::lists('price')->all();
    return [
        'order_id'=>$faker->randomElement($orders),
        'product_name'=>$faker->randomElement($products),
        'boission'=>$faker->randomElement($products),
        'riz'=>'Riz blanc',
        'price'=>$faker->randomElement($prices),
        'qty'=>$faker->numberBetween(2,10)
        
    ];
});