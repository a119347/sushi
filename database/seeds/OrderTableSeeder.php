<?php

use App\Orders;
use Illuminate\Database\Seeder;

class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Orders::truncate();
        factory(App\Orders::class,100)->create();
    }
}
