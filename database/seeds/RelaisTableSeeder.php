<?php

use App\Relais;
use Illuminate\Database\Seeder;

class RelaisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Relais::truncate();
        factory(App\Relais::class,20)->create();
    }
}
