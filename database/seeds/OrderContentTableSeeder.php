<?php

use App\OrderContent;
use Illuminate\Database\Seeder;

class OrderContentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        OrderContent::truncate();
        factory(App\OrderContent::class,200)->create();
    }
}
